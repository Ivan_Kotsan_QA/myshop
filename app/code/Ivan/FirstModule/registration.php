<?php


namespace Ivan\FirstModule;

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Ivan_FirstModule', __DIR__);

