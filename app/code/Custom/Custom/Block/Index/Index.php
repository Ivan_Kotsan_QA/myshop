<?php
declare(strict_types=1);

namespace Custom\Custom\Block\Index;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrderBuilder;

class Index extends \Magento\Framework\View\Element\Template
{

    protected $productRepository;

    protected $searchCriteriaBuilder;

    protected $sortOrderBuilder;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param ProductRepositoryInterface $productRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SortOrderBuilder $sortOrderBuilder
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
    }

    public function getProductById($id)
    {
        return $this->productRepository->getById($id);
    }

    public function getProducts()
    {
//        return  $this->productRepository->getList($this->searchCriteriaBuilder->create())->getItems();
        return $this->productRepository->getList(
            $this->searchCriteriaBuilder
                ->addFilter('entity_id', 620, 'gt')
                ->addSortOrder(
                    $this->sortOrderBuilder->setField('entity_id')->setAscendingDirection()->create()
                )
                ->create()
        )->getItems();
    }
}

