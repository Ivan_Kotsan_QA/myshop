<?php


namespace Blog\Articles\Plugin;


class ArticlesPlugin
{
    public function afterGetArticles($subject, $result)
    {
        $pp = $result->getItems();
        unset($pp[2]);
        $result->setItems($pp);
        return $result;

    }

    public function aroundGetArticles($subject, $proceed)
    {
        $a = 1;
        $test = $proceed();
        $b = 2;
        return $test;
    }
}
