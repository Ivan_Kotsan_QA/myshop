<?php

namespace Blog\Articles\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ArticlesAction implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $test = $observer->getData("articles");
        $ppp = $test->getItems();
        unset($ppp[1]);
        $test->setItems($ppp);
        $observer->setData("articles", $test);
    }
}
