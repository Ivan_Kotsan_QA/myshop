<?php

namespace Blog\Articles\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;

class ArticlesHelper extends AbstractHelper
{
    protected $storeManager;
    public function __construct(Context $context, StoreManagerInterface $storeManager)
    {
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    public function Test()
    {
        return $this->scopeConfig->getValue("articles/settings/settings", "store", $this->storeManager->getStore()->getId());
    }
    public function Test2()
    {
        return $this->scopeConfig->getValue("articles/settings/text", "store", $this->storeManager->getStore()->getId());
    }
    public function Test3()
    {
        return $this->scopeConfig->getValue("articles/settings/articlesSetting", "store", $this->storeManager->getStore()->getId());
    }
}
