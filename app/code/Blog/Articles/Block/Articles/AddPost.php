<?php

namespace Blog\Articles\Block\Articles;

use Blog\Articles\Api\ArticlesRepositoryInterface;
use Blog\Articles\Helper\ArticlesHelper;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\View\Element\Template;

class AddPost extends Template
{
    protected $articlesRepository;
    protected $searchCriteriaBuilder;
    protected $articlesHelper;

    public function __construct(
        Template\Context $context,
        ArticlesRepositoryInterface $articlesRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ArticlesHelper $articlesHelper,
        array $data = []
    ) {
        $this->articlesHelper = $articlesHelper;
        $this->articlesRepository = $articlesRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        parent::__construct($context, $data);
    }
    public function getArticles()
    {
        $test = $this->articlesRepository->getList($this->searchCriteriaBuilder->create());
        $this->_eventManager->dispatch("blog_articles_getArticles", ["articles" => $test]);
        return $test;
    }

    public function getHelper()
    {
        return $this->articlesHelper;
    }

}
