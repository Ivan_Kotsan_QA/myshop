<?php
declare(strict_types=1);

namespace Blog\Articles\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ArticlesRepositoryInterface
{

    /**
     * Save Articles
     * @param \Blog\Articles\Api\Data\ArticlesInterface $articles
     * @return \Blog\Articles\Api\Data\ArticlesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Blog\Articles\Api\Data\ArticlesInterface $articles
    );

    /**
     * Retrieve Articles
     * @param string $articlesId
     * @return \Blog\Articles\Api\Data\ArticlesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($articlesId);

    /**
     * Retrieve Articles matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Blog\Articles\Api\Data\ArticlesSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Articles
     * @param \Blog\Articles\Api\Data\ArticlesInterface $articles
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Blog\Articles\Api\Data\ArticlesInterface $articles
    );

    /**
     * Delete Articles by ID
     * @param string $articlesId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($articlesId);
}

