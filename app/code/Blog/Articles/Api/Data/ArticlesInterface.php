<?php
declare(strict_types=1);

namespace Blog\Articles\Api\Data;

interface ArticlesInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const AUTHOR = 'author';
    const TITLE = 'title';
    const DATE = 'date';
    const ARTICLES_ID = 'articles_id';
    const CONTENT = 'content';

    /**
     * Get articles_id
     * @return string|null
     */
    public function getArticlesId();

    /**
     * Set articles_id
     * @param string $articlesId
     * @return \Blog\Articles\Api\Data\ArticlesInterface
     */
    public function setArticlesId($articlesId);

    /**
     * Get title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     * @param string $title
     * @return \Blog\Articles\Api\Data\ArticlesInterface
     */
    public function setTitle($title);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Blog\Articles\Api\Data\ArticlesExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Blog\Articles\Api\Data\ArticlesExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Blog\Articles\Api\Data\ArticlesExtensionInterface $extensionAttributes
    );

    /**
     * Get content
     * @return string|null
     */
    public function getContent();

    /**
     * Set content
     * @param string $content
     * @return \Blog\Articles\Api\Data\ArticlesInterface
     */
    public function setContent($content);

    /**
     * Get date
     * @return string|null
     */
    public function getDate();

    /**
     * Set date
     * @param string $date
     * @return \Blog\Articles\Api\Data\ArticlesInterface
     */
    public function setDate($date);

    /**
     * Get author
     * @return string|null
     */
    public function getAuthor();

    /**
     * Set author
     * @param string $author
     * @return \Blog\Articles\Api\Data\ArticlesInterface
     */
    public function setAuthor($author);
}

