<?php
declare(strict_types=1);

namespace Blog\Articles\Api\Data;

interface ArticlesSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Articles list.
     * @return \Blog\Articles\Api\Data\ArticlesInterface[]
     */
    public function getItems();

    /**
     * Set title list.
     * @param \Blog\Articles\Api\Data\ArticlesInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

