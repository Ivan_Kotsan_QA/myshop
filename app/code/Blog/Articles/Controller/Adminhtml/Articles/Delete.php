<?php
declare(strict_types=1);

namespace Blog\Articles\Controller\Adminhtml\Articles;

class Delete extends \Blog\Articles\Controller\Adminhtml\Articles
{

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('articles_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\Blog\Articles\Model\Articles::class);
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Articles.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['articles_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Articles to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}

