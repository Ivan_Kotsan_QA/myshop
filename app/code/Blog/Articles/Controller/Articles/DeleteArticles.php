<?php

namespace Blog\Articles\Controller\Articles;

use Blog\Articles\Api\ArticlesRepositoryInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class DeleteArticles extends Action
{
    protected $logger;
    protected $articlesRepository;
    public function __construct(
        Context $context,
        ArticlesRepositoryInterface $articlesRepository,
        \Blog\Articles\Logger\Logger $logger
    ) {
        $this->logger = $logger;
        $this->articlesRepository = $articlesRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam("id");
        if (is_numeric($id)) {
            try {
                $this->articlesRepository->deleteById($id);
            } catch (\Exception $exception) {
                $this->logger->error($exception->getMessage());
            }
            return  $this->_redirect($this->_redirect->getRefererUrl());
        }

        return $this->_forward("noroute");
    }
}
