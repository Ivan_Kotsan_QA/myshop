<?php
declare(strict_types=1);

namespace Blog\Articles\Model;

use Blog\Articles\Api\Data\ArticlesInterface;
use Blog\Articles\Api\Data\ArticlesInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Articles extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'blog_articles_articles';
    protected $articlesDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ArticlesInterfaceFactory $articlesDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Blog\Articles\Model\ResourceModel\Articles $resource
     * @param \Blog\Articles\Model\ResourceModel\Articles\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ArticlesInterfaceFactory $articlesDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Blog\Articles\Model\ResourceModel\Articles $resource,
        \Blog\Articles\Model\ResourceModel\Articles\Collection $resourceCollection,
        array $data = []
    ) {
        $this->articlesDataFactory = $articlesDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve articles model with articles data
     * @return ArticlesInterface
     */
    public function getDataModel()
    {
        $articlesData = $this->getData();
        
        $articlesDataObject = $this->articlesDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $articlesDataObject,
            $articlesData,
            ArticlesInterface::class
        );
        
        return $articlesDataObject;
    }
}

