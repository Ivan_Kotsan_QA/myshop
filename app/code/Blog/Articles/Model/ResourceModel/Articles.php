<?php
declare(strict_types=1);

namespace Blog\Articles\Model\ResourceModel;

class Articles extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('blog_articles_articles', 'articles_id');
    }
}

