<?php
declare(strict_types=1);

namespace Blog\Articles\Model\ResourceModel\Articles;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'articles_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Blog\Articles\Model\Articles::class,
            \Blog\Articles\Model\ResourceModel\Articles::class
        );
    }
}

