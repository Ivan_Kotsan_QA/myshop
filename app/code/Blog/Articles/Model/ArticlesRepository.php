<?php
declare(strict_types=1);

namespace Blog\Articles\Model;

use Blog\Articles\Api\ArticlesRepositoryInterface;
use Blog\Articles\Api\Data\ArticlesInterfaceFactory;
use Blog\Articles\Api\Data\ArticlesSearchResultsInterfaceFactory;
use Blog\Articles\Model\ResourceModel\Articles as ResourceArticles;
use Blog\Articles\Model\ResourceModel\Articles\CollectionFactory as ArticlesCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class ArticlesRepository implements ArticlesRepositoryInterface
{

    protected $extensibleDataObjectConverter;
    protected $dataObjectHelper;

    protected $resource;

    private $storeManager;

    protected $articlesFactory;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $articlesCollectionFactory;

    protected $dataArticlesFactory;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;


    /**
     * @param ResourceArticles $resource
     * @param ArticlesFactory $articlesFactory
     * @param ArticlesInterfaceFactory $dataArticlesFactory
     * @param ArticlesCollectionFactory $articlesCollectionFactory
     * @param ArticlesSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceArticles $resource,
        ArticlesFactory $articlesFactory,
        ArticlesInterfaceFactory $dataArticlesFactory,
        ArticlesCollectionFactory $articlesCollectionFactory,
        ArticlesSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->articlesFactory = $articlesFactory;
        $this->articlesCollectionFactory = $articlesCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataArticlesFactory = $dataArticlesFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Blog\Articles\Api\Data\ArticlesInterface $articles
    ) {
        /* if (empty($articles->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $articles->setStoreId($storeId);
        } */
        
        $articlesData = $this->extensibleDataObjectConverter->toNestedArray(
            $articles,
            [],
            \Blog\Articles\Api\Data\ArticlesInterface::class
        );
        
        $articlesModel = $this->articlesFactory->create()->setData($articlesData);
        
        try {
            $this->resource->save($articlesModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the articles: %1',
                $exception->getMessage()
            ));
        }
        return $articlesModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($articlesId)
    {
        $articles = $this->articlesFactory->create();
        $this->resource->load($articles, $articlesId);
        if (!$articles->getId()) {
            throw new NoSuchEntityException(__('Articles with id "%1" does not exist.', $articlesId));
        }
        return $articles->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->articlesCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Blog\Articles\Api\Data\ArticlesInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Blog\Articles\Api\Data\ArticlesInterface $articles
    ) {
        try {
            $articlesModel = $this->articlesFactory->create();
            $this->resource->load($articlesModel, $articles->getArticlesId());
            $this->resource->delete($articlesModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Articles: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($articlesId)
    {
        return $this->delete($this->get($articlesId));
    }
}

