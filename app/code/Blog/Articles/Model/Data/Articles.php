<?php
declare(strict_types=1);

namespace Blog\Articles\Model\Data;

use Blog\Articles\Api\Data\ArticlesInterface;

class Articles extends \Magento\Framework\Api\AbstractExtensibleObject implements ArticlesInterface
{

    /**
     * Get articles_id
     * @return string|null
     */
    public function getArticlesId()
    {
        return $this->_get(self::ARTICLES_ID);
    }

    /**
     * Set articles_id
     * @param string $articlesId
     * @return \Blog\Articles\Api\Data\ArticlesInterface
     */
    public function setArticlesId($articlesId)
    {
        return $this->setData(self::ARTICLES_ID, $articlesId);
    }

    /**
     * Get title
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * Set title
     * @param string $title
     * @return \Blog\Articles\Api\Data\ArticlesInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Blog\Articles\Api\Data\ArticlesExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Blog\Articles\Api\Data\ArticlesExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Blog\Articles\Api\Data\ArticlesExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get content
     * @return string|null
     */
    public function getContent()
    {
        return $this->_get(self::CONTENT);
    }

    /**
     * Set content
     * @param string $content
     * @return \Blog\Articles\Api\Data\ArticlesInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Get date
     * @return string|null
     */
    public function getDate()
    {
        return $this->_get(self::DATE);
    }

    /**
     * Set date
     * @param string $date
     * @return \Blog\Articles\Api\Data\ArticlesInterface
     */
    public function setDate($date)
    {
        return $this->setData(self::DATE, $date);
    }

    /**
     * Get author
     * @return string|null
     */
    public function getAuthor()
    {
        return $this->_get(self::AUTHOR);
    }

    /**
     * Set author
     * @param string $author
     * @return \Blog\Articles\Api\Data\ArticlesInterface
     */
    public function setAuthor($author)
    {
        return $this->setData(self::AUTHOR, $author);
    }
}

