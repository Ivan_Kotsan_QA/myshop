<?php

namespace Kotsan\Blog\Ui\Component\Listing\Column;

use Kotsan\Blog\Api\CategoryRepositoryInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class ParentCategory extends Column
{
    protected $categoryRepository;
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        CategoryRepositoryInterface $categoryRepository,
        array $components = [],
        array $data = []
    ) {
        $this->categoryRepository = $categoryRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $result = false;
                if ($item[$this->getData('name')]) {
                    try {
                        $category =  $this->categoryRepository->get($item[$this->getData('name')]);
                        if ($category) {
                            $item[$this->getData('name')] = $category->getTitle();
                            $result = true;
                        }
                    } catch (\Exception $exception) {
                    }
                }
                if (!$result) {
                    $item[$this->getData('name')] = "";
                }
            }
        }

        return $dataSource;
    }
}
