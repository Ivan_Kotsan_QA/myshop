<?php

namespace Kotsan\Blog\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;

class YesNo extends Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item[$this->getData('name')] = $item[$this->getData('name')] ? __("Yes") : __("No");
            }
        }

        return $dataSource;
    }
}
