<?php

namespace Kotsan\Blog\Block\Post;

use Kotsan\Blog\Api\PostRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Page\Config;
use Magento\Framework\View\Result\PageFactory;

class Show extends Template
{
    protected $postRepository;
    protected $searchCriteriaBuilder;
    protected $_filterProvider;
    public function __construct(
        Template\Context $context,
        PostRepositoryInterface $postRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        array $data = []
    ) {
        $this->_filterProvider = $filterProvider;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->postRepository = $postRepository;
        parent::__construct($context, $data);
    }

    public function showAllPosts()
    {
        return $this->postRepository->getList($this->searchCriteriaBuilder->create())->getItems();
    }
    public function showPostById()
    {
        $id = $this->getRequest()->getParam('id');
        return $this->postRepository->get($id);
    }
    public function getCmsFilterContent($value='')
    {
        return $this->_filterProvider->getPageFilter()->filter($value);

    }
}
