<?php
declare(strict_types=1);

namespace Kotsan\Blog\Api\Data;

interface PostInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const META_KEYWORDS = 'meta_keywords';
    const POST_ID = 'post_id';
    const TITLE = 'title';
    const URL = 'url';
    const CONTENT = 'content';
    const STATUS = 'status';
    const META_DESCRIPTION = 'meta_description';
    const SHORT_CONTENT = 'short_content';
    const TIMESTAMP_INIT = 'created_datetime';
    const TIMESTAMP_INIT_UPDATE = 'updated_datetime';

    /**
     * Get post_id
     * @return string|null
     */
    public function getPostId();

    /**
     * @return mixed
     */
    public function getId();

    /**
     * Set post_id
     * @param string $postId
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setPostId($postId);

    /**
     * @param $postId
     * @return mixed
     */
    public function setId($postId);

    /**
     * Get title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     * @param string $title
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setTitle($title);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kotsan\Blog\Api\Data\PostExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Kotsan\Blog\Api\Data\PostExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kotsan\Blog\Api\Data\PostExtensionInterface $extensionAttributes
    );

    /**
     * Get short_content
     * @return string|null
     */
    public function getShortContent();

    /**
     * Set short_content
     * @param string $shortContent
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setShortContent($shortContent);

    /**
     * Get content
     * @return string|null
     */
    public function getContent();

    /**
     * Set content
     * @param string $content
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setContent($content);

    /**
     * Get meta_keywords
     * @return string|null
     */
    public function getMetaKeywords();

    /**
     * Set meta_keywords
     * @param string $metaKeywords
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setMetaKeywords($metaKeywords);

    /**
     * Get meta_description
     * @return string|null
     */
    public function getMetaDescription();

    /**
     * Set meta_description
     * @param string $metaDescription
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setMetaDescription($metaDescription);

    /**
     * Get url
     * @return string|null
     */
    public function getUrl();

    /**
     * Set url
     * @param string $url
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setUrl($url);

    /**
     * Get status
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     * @param string $status
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setStatus($status);

    /**
     * @return mixed
     */
    public function getCreatedDatetime();

    /**
     * @param string $setData
     * @return mixed
     */
    public function setCreatedDatetime($setData);

    /**
     * @param $setUpdateData
     * @return mixed
     */
    public function setUpdatedDatetime($setUpdateData);

    /**
     * @return mixed
     */
    public function getUpdatedDatetime();
}
