<?php
declare(strict_types=1);

namespace Kotsan\Blog\Api\Data;

interface CategoryInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const SORT_ORDER = 'sort_order';
    const META_KEYWORDS = 'meta_keywords';
    const PARENT_CATEGORY = 'parent_category';
    const TITLE = 'title';
    const CATEGORY_ID = 'category_id';
    const URL = 'url';
    const CONTENT = 'content';
    const STATUS = 'status';
    const META_DESCRIPTION = 'meta_description';
    const SHORT_CONTENT = 'short_content';
    const TIMESTAMP_INIT = 'created_datetime';
    const TIMESTAMP_INIT_UPDATE = 'updated_datetime';

    /**
     * Get category_id
     * @return string|null
     */
    public function getCategoryId();

    /**
     * @return mixed
     */
    public function getId();

    /**
     * Set category_id
     * @param string $categoryId
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setCategoryId($categoryId);

    /**
     * @param $categoryId
     * @return mixed
     */
    public function setId($categoryId);

    /**
     * Get title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     * @param string $title
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setTitle($title);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kotsan\Blog\Api\Data\CategoryExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Kotsan\Blog\Api\Data\CategoryExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kotsan\Blog\Api\Data\CategoryExtensionInterface $extensionAttributes
    );

    /**
     * Get meta_keywords
     * @return string|null
     */
    public function getMetaKeywords();

    /**
     * Set meta_keywords
     * @param string $metaKeywords
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setMetaKeywords($metaKeywords);

    /**
     * Get meta_description
     * @return string|null
     */
    public function getMetaDescription();

    /**
     * Set meta_description
     * @param string $metaDescription
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setMetaDescription($metaDescription);

    /**
     * Get url
     * @return string|null
     */
    public function getUrl();

    /**
     * Set url
     * @param string $url
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setUrl($url);

    /**
     * Get parent_category
     * @return string|null
     */
    public function getParentCategory();

    /**
     * Set parent_category
     * @param string $parentCategory
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setParentCategory($parentCategory);

    /**
     * Get sort_order
     * @return string|null
     */
    public function getSortOrder();

    /**
     * Set sort_order
     * @param string $sortOrder
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setSortOrder($sortOrder);

    /**
     * Get content
     * @return string|null
     */
    public function getContent();

    /**
     * Set content
     * @param string $content
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setContent($content);

    /**
     * Get short_content
     * @return string|null
     */
    public function getShortContent();

    /**
     * Set short_content
     * @param string $shortContent
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setShortContent($shortContent);

    /**
     * Get status
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     * @param string $status
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setStatus($status);

    /**
     * @param $setData
     * @return mixed
     */
    public function setCreatedDatetime($setData);

    /**
     * @return mixed
     */
    public function getCreatedDatetime();

    /**
     * @param $setUpdateData
     * @return mixed
     */
    public function setUpdateDatetime($setUpdateData);

    /**
     * @return mixed
     */
    public function getUpdateDatetime();
}
