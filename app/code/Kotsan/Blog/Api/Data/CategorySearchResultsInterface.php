<?php
declare(strict_types=1);

namespace Kotsan\Blog\Api\Data;

interface CategorySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Category list.
     * @return \Kotsan\Blog\Api\Data\CategoryInterface[]
     */
    public function getItems();

    /**
     * Set title list.
     * @param \Kotsan\Blog\Api\Data\CategoryInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

