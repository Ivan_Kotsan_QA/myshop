<?php
declare(strict_types=1);

namespace Kotsan\Blog\Api\Data;

interface PostSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Post list.
     * @return \Kotsan\Blog\Api\Data\PostInterface[]
     */
    public function getItems();

    /**
     * Set title list.
     * @param \Kotsan\Blog\Api\Data\PostInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

