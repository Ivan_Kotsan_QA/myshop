<?php

namespace Kotsan\Blog\Model\Config\Source;

use Kotsan\Blog\Model\CategoryRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;

class Category implements \Magento\Framework\Data\OptionSourceInterface
{
    protected $searchCriteriaBuilder;
    protected $categoryRepository;
    public function __construct(
        CategoryRepository $categoryRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function toOptionArray()
    {
        $options = [['label' => __('-- Please Select --'), 'value' => '']];
        foreach ($this->categoryRepository->getList($this->searchCriteriaBuilder->create())->getItems() as $category) {
            $options[] = ["label" => $category->getTitle(), "value" => $category->getId()];
        }
        return $options;
    }
}
