<?php
declare(strict_types=1);

namespace Kotsan\Blog\Model\ResourceModel;

class Category extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('kotsan_blog_category', 'category_id');
    }
}

