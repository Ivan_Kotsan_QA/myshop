<?php
declare(strict_types=1);

namespace Kotsan\Blog\Model\ResourceModel\Post;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'post_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Kotsan\Blog\Model\Post::class,
            \Kotsan\Blog\Model\ResourceModel\Post::class
        );
    }
}

