<?php
declare(strict_types=1);

namespace Kotsan\Blog\Model;

use Kotsan\Blog\Api\Data\CategoryInterface;
use Kotsan\Blog\Api\Data\CategoryInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Category extends \Magento\Framework\Model\AbstractModel
{
    protected $dataObjectHelper;

    protected $categoryDataFactory;

    protected $_eventPrefix = 'kotsan_blog_category';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param CategoryInterfaceFactory $categoryDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Kotsan\Blog\Model\ResourceModel\Category $resource
     * @param \Kotsan\Blog\Model\ResourceModel\Category\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        CategoryInterfaceFactory $categoryDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Kotsan\Blog\Model\ResourceModel\Category $resource,
        \Kotsan\Blog\Model\ResourceModel\Category\Collection $resourceCollection,
        array $data = []
    ) {
        $this->categoryDataFactory = $categoryDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve category model with category data
     * @return CategoryInterface
     */
    public function getDataModel()
    {
        $categoryData = $this->getData();

        $categoryDataObject = $this->categoryDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $categoryDataObject,
            $categoryData,
            CategoryInterface::class
        );

        return $categoryDataObject;
    }
}
