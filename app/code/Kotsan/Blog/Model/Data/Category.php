<?php
declare(strict_types=1);

namespace Kotsan\Blog\Model\Data;

use Kotsan\Blog\Api\Data\CategoryInterface;

class Category extends \Magento\Framework\Api\AbstractExtensibleObject implements CategoryInterface
{

    /**
     * Get category_id
     * @return string|null
     */
    public function getCategoryId()
    {
        return $this->_get(self::CATEGORY_ID);
    }

    /**
     * Set category_id
     * @param string $categoryId
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setCategoryId($categoryId)
    {
        return $this->setData(self::CATEGORY_ID, $categoryId);
    }

    /**
     * @return mixed|string|null
     */
    public function getId()
    {
        return $this->getCategoryId();
    }

    /**
     * @param $categoryId
     * @return CategoryInterface|mixed
     */
    public function setId($categoryId)
    {
        return $this->setCategoryId($categoryId);
    }

    /**
     * Get title
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * Set title
     * @param string $title
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kotsan\Blog\Api\Data\CategoryExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Kotsan\Blog\Api\Data\CategoryExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kotsan\Blog\Api\Data\CategoryExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get meta_keywords
     * @return string|null
     */
    public function getMetaKeywords()
    {
        return $this->_get(self::META_KEYWORDS);
    }

    /**
     * Set meta_keywords
     * @param string $metaKeywords
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setMetaKeywords($metaKeywords)
    {
        return $this->setData(self::META_KEYWORDS, $metaKeywords);
    }

    /**
     * Get meta_description
     * @return string|null
     */
    public function getMetaDescription()
    {
        return $this->_get(self::META_DESCRIPTION);
    }

    /**
     * Set meta_description
     * @param string $metaDescription
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setMetaDescription($metaDescription)
    {
        return $this->setData(self::META_DESCRIPTION, $metaDescription);
    }

    /**
     * Get url
     * @return string|null
     */
    public function getUrl()
    {
        return $this->_get(self::URL);
    }

    /**
     * Set url
     * @param string $url
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setUrl($url)
    {
        return $this->setData(self::URL, $url);
    }

    /**
     * Get parent_category
     * @return string|null
     */
    public function getParentCategory()
    {
        return $this->_get(self::PARENT_CATEGORY);
    }

    /**
     * Set parent_category
     * @param string $parentCategory
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setParentCategory($parentCategory)
    {
        return $this->setData(self::PARENT_CATEGORY, $parentCategory);
    }

    /**
     * Get sort_order
     * @return string|null
     */
    public function getSortOrder()
    {
        return $this->_get(self::SORT_ORDER);
    }

    /**
     * Set sort_order
     * @param string $sortOrder
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }

    /**
     * Get content
     * @return string|null
     */
    public function getContent()
    {
        return $this->_get(self::CONTENT);
    }

    /**
     * Set content
     * @param string $content
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Get short_content
     * @return string|null
     */
    public function getShortContent()
    {
        return $this->_get(self::SHORT_CONTENT);
    }

    /**
     * Set short_content
     * @param string $shortContent
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setShortContent($shortContent)
    {
        return $this->setData(self::SHORT_CONTENT, $shortContent);
    }

    /**
     * Get status
     * @return string|null
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * Set status
     * @param string $status
     * @return \Kotsan\Blog\Api\Data\CategoryInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @param $setData
     * @return Category|mixed
     */
    public function setCreatedDatetime($setData)
    {
        return $this->setData(self::TIMESTAMP_INIT, $setData);
    }

    /**
     * @param $getData
     * @return Category|mixed
     */
    public function getCreatedDatetime()
    {
        return $this->_get(self::TIMESTAMP_INIT);
    }

    /**
     * @param $setUpdateData
     * @return Category|mixed
     */
    public function setUpdateDatetime($setUpdateData)
    {
        return $this->setData(self::TIMESTAMP_INIT_UPDATE, $setUpdateData);
    }
    public function getUpdateDatetime()
    {
        return $this->_get(self::TIMESTAMP_INIT_UPDATE);
    }
}
