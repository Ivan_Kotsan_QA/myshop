<?php
declare(strict_types=1);

namespace Kotsan\Blog\Model\Data;

use Kotsan\Blog\Api\Data\PostInterface;


class Post extends \Magento\Framework\Api\AbstractExtensibleObject implements PostInterface
{

    /**
     * Get post_id
     * @return string|null
     */
    public function getPostId()
    {
        return $this->_get(self::POST_ID);
    }

    /**
     * @return mixed|string|null
     */
    public function getId()
    {
        return $this->getPostId();
    }

    /**
     * Set post_id
     * @param string $postId
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setPostId($postId)
    {
        return $this->setData(self::POST_ID, $postId);
    }

    /**
     * @param $id
     * @return PostInterface|mixed
     */
    public function setId($id)
    {
        return $this->setPostId($id);
    }

    /**
     * Get title
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * Set title
     * @param string $title
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kotsan\Blog\Api\Data\PostExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Kotsan\Blog\Api\Data\PostExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kotsan\Blog\Api\Data\PostExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get short_content
     * @return string|null
     */
    public function getShortContent()
    {
        return $this->_get(self::SHORT_CONTENT);
    }

    /**
     * @return mixed|null
     */
    public function getDate()
    {
        return $this->_get(self::TIMESTAMP_INIT);
    }

    /**
     * Set short_content
     * @param string $shortContent
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setShortContent($shortContent)
    {
        return $this->setData(self::SHORT_CONTENT, $shortContent);
    }

    /**
     * Get content
     * @return string|null
     */
    public function getContent()
    {
        return $this->_get(self::CONTENT);
    }

    /**
     * Set content
     * @param string $content
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Get meta_keywords
     * @return string|null
     */
    public function getMetaKeywords()
    {
        return $this->_get(self::META_KEYWORDS);
    }

    /**
     * Set meta_keywords
     * @param string $metaKeywords
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setMetaKeywords($metaKeywords)
    {
        return $this->setData(self::META_KEYWORDS, $metaKeywords);
    }

    /**
     * Get meta_description
     * @return string|null
     */
    public function getMetaDescription()
    {
        return $this->_get(self::META_DESCRIPTION);
    }

    /**
     * Set meta_description
     * @param string $metaDescription
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setMetaDescription($metaDescription)
    {
        return $this->setData(self::META_DESCRIPTION, $metaDescription);
    }

    /**
     * Get url
     * @return string|null
     */
    public function getUrl()
    {
        return $this->_get(self::URL);
    }

    /**
     * Set url
     * @param string $url
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setUrl($url)
    {
        return $this->setData(self::URL, $url);
    }

    /**
     * Get status
     * @return string|null
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * Set status
     * @param string $status
     * @return \Kotsan\Blog\Api\Data\PostInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @param $setData
     * @return Post|mixed
     */
    public function setCreatedDatetime($setData)
    {
        return $this->setData(self::TIMESTAMP_INIT, $setData);
    }

    /**
     * @param $getData
     * @return Post|mixed
     */
    public function getCreatedDatetime()
    {
        return $this->_get(self::TIMESTAMP_INIT);
    }

    /**
     * @param $setUpdatedData
     * @return Post|mixed
     */
    public function setUpdatedDatetime($setUpdatedData)
    {
        return $this->setData(self::TIMESTAMP_INIT_UPDATE, $setUpdatedData);
    }

    /**
     * @return Post|mixed
     */
    public function getUpdatedDatetime()
    {
        return $this->_get(self::TIMESTAMP_INIT_UPDATE);
    }
}
