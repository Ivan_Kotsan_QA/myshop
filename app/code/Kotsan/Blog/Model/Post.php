<?php
declare(strict_types=1);

namespace Kotsan\Blog\Model;

use Kotsan\Blog\Api\Data\PostInterface;
use Kotsan\Blog\Api\Data\PostInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Post extends \Magento\Framework\Model\AbstractModel
{
    protected $dataObjectHelper;

    protected $postDataFactory;

    protected $_eventPrefix = 'kotsan_blog_post';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PostInterfaceFactory $postDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Kotsan\Blog\Model\ResourceModel\Post $resource
     * @param \Kotsan\Blog\Model\ResourceModel\Post\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PostInterfaceFactory $postDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Kotsan\Blog\Model\ResourceModel\Post $resource,
        \Kotsan\Blog\Model\ResourceModel\Post\Collection $resourceCollection,
        array $data = []
    ) {
        $this->postDataFactory = $postDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve post model with post data
     * @return PostInterface
     */
    public function getDataModel()
    {
        $postData = $this->getData();

        $postDataObject = $this->postDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $postDataObject,
            $postData,
            PostInterface::class
        );

        return $postDataObject;
    }
}
