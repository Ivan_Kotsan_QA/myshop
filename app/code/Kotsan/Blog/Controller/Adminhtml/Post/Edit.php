<?php
declare(strict_types=1);

namespace Kotsan\Blog\Controller\Adminhtml\Post;

use Kotsan\Blog\Api\PostRepositoryInterface;
use Kotsan\Blog\Model\PostFactory;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends \Kotsan\Blog\Controller\Adminhtml\Post
{
    protected $resultPageFactory;
    protected $postRepository;
    protected $postFactory;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     * @param PostRepositoryInterface $postRepository
     * @param PostFactory $postFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        PostRepositoryInterface $postRepository,
        PostFactory $postFactory
    ) {
        $this->postFactory = $postFactory;
        $this->postRepository = $postRepository;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('post_id');
        $error = false;

        // 2. Initial checking
        if (is_numeric($id)) {
            $model = $this->postRepository->get($id);
            if (!is_numeric($model->getPostId())) {
                $error = true;
            }
        } else {
            $model = $this->postFactory->create();
        }
        if ($error) {
            $this->messageManager->addErrorMessage(__('This Post no longer exists.'));
            /** @var Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/');
        }

        $this->_coreRegistry->register('kotsan_blog_post', $model);

        // 3. Build edit form
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Post') : __('New Post'),
            $id ? __('Edit Post') : __('New Post')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Posts'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Post %1', $model->getId()) : __('New Post'));
        return $resultPage;
    }
}
