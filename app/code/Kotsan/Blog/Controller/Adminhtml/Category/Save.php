<?php
declare(strict_types=1);

namespace Kotsan\Blog\Controller\Adminhtml\Category;

use Kotsan\Blog\Api\CategoryRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{
    protected $dataPersistor;
    protected $categoryRepository;
    protected $categoryFactory;
    /**
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        CategoryRepositoryInterface $categoryRepository,
        \Kotsan\Blog\Model\CategoryFactory $categoryFactory
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->dataPersistor = $dataPersistor;
        $this->categoryFactory = $categoryFactory;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('category_id');
            if (is_numeric($id)) {
                try {
                    $model = $this->categoryRepository->get($id);
                }  catch (\Exception $exception){
                    $this->messageManager->addErrorMessage(__('This Category no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            } else {
                $model = $this->categoryFactory->create();
            }
            /** @var \Kotsan\Blog\Model\Category $model */
//            $model = $model->getDataModel();
            $model->setData($data);

            try {
                $this->categoryRepository->save($model->getDataModel());
                $this->messageManager->addSuccessMessage(__('You saved the Category.'));
                $this->dataPersistor->clear('kotsan_blog_category');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['category_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Category.'));
            }

            $this->dataPersistor->set('kotsan_blog_category', $data);
            return $resultRedirect->setPath('*/*/edit', ['category_id' => $this->getRequest()->getParam('category_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
