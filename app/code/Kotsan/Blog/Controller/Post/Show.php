<?php

namespace Kotsan\Blog\Controller\Post;

use Kotsan\Blog\Api\PostRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Controller\ResultInterface;

class Show extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $postRepository;
    protected $searchCriteriaBuilder;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param PostRepositoryInterface $postRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        PostRepositoryInterface $postRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->postRepository = $postRepository;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return ResultInterface|void
     */
    public function execute()
    {
        $result= $this->resultPageFactory->create();
        $id = $this->getRequest()->getParam('id');
        if (is_numeric($id)) {
            try {
                $post = $this->postRepository->get($id);
                $result->getConfig()->getTitle()->set($post->getTitle()); //setting the page
                $result->getConfig()->setDescription($post->getMetaDescription()); // set meta description
                $result->getConfig()->setKeywords($post->getMetaKeywords()); // set meta keyword
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage(__('Tакой статьи не существует.'));
                $this->_forward("noroute");
            }
        } else {
            $this->messageManager->addErrorMessage(__('Tакой страници не существует.'));
            $this->_forward("noroute");
        }
        return $result;
    }
}
